defmodule Aoc2018Test do
  use ExUnit.Case
  doctest Aoc2018

  test "greets the world" do
    assert Aoc2018.hello() == :world
  end

  test "should add positive number" do
    assert Aoc2018.calibrate('+10', 0) == 10
  end
  
end
