defmodule Aoc2018 do
  @moduledoc """
  Documentation for Aoc2018.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Aoc2018.hello()
      :world
      
      iex> File.read("input/p01.txt") |> elem(1) |> Aoc2018.dayone
      522
  """
  def hello do
    :world
  end

  def dayone(input) do
    input
    |> String.split
    |> Enum.map(fn n -> String.to_integer(n) end)
    |> Enum.sum
  end

end
